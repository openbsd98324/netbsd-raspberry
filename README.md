# netbsd-raspberry

https://ftp.netbsd.org/pub/


573e75874b4b04c2138833f7ab29f007  cmretropie-4.5.1-rpi2_rpi3.img.gz 
https://gitlab.com/openbsd98324/cmretropie/-/raw/main/cmretropie-4.5.1-rpi2_rpi3.img.gz

https://gitlab.com/openbsd98324/armv7/-/raw/main/v2-custom/image.armv7hf.img.gz


"http://ftp.netbsd.org/pub/NetBSD/NetBSD-9.1/evbarm-earmv7hf/binary/gzimg/armv7.img.gz" 

````
evbarm-aarch64/	04-Aug-2022 19:04	1kB
evbarm-earm/	04-Aug-2022 18:34	1kB
evbarm-earmeb/	04-Aug-2022 18:39	1kB
evbarm-earmhf/	04-Aug-2022 19:04	1kB    
evbarm-earmhfeb/	04-Aug-2022 18:41	1kB
evbarm-earmv6hf/	04-Aug-2022 18:48	1kB
evbarm-earmv7hf/	04-Aug-2022 19:03	1kB
evbarm-earmv7hfeb/	04-Aug-2022 18:47	1k
````

````
RPI1, RPI2, RPI2-1.2, RPI3, RPI3+ (except RPI3 builtin WiFi and bluetooth)
RPI0 and RPI0W are expected to work (without WiFi, and one needs fdt files \todo where from?)
multiple processors on RPI2/RPI3
````
https://wiki.netbsd.org/ports/evbarm/raspberry_pi/

Pi 0 and 1 
http://nycdn.netbsd.org/pub/NetBSD-daily/netbsd-9/latest/evbarm-earmv6hf/binary/gzimg/rpi.img.gz

Pi 2-3
https://nycdn.netbsd.org/pub/NetBSD-daily/netbsd-9/latest/evbarm-earmv7hf/binary/gzimg/armv7.img.gz

Pi 3
https://nycdn.netbsd.org/pub/NetBSD-daily/netbsd-9/latest/evbarm-earmv7hf/binary/gzimg/armv7.img.gz
https://nycdn.netbsd.org/pub/NetBSD-daily/netbsd-9/latest/evbarm-aarch64/binary/gzimg/arm64.img.gz



# Using standard images
The simplest way is to download the appropriate SD card image from the NetBSD mirrors:

The Raspberry Pi 1 requires the ARMv6 rpi.img.gz.
The Raspberry Pi 2-3 can use the standard ARMv7 armv7.img.gz image.
The Raspberry Pi 3 can also use arm64.img.gz.


# CPU types

RPI1 uses "earmv6hf".
RPI0 uses "earmv6hf".
RPI0W uses "earmv6hf".
RPI2 uses "earmv7hf".
RPI2-1.2 uses "earmv7hf" or "aarch64" (ARMv8 CPU hardware)
RPI3 uses "earmv7hf" or "aarch64" (ARMv8 CPU hardware)
RPI4 uses "aarch64" (ARMv8 CPU hardware)

# Image
http://nycdn.netbsd.org/pub/NetBSD-daily/netbsd-9/202302022250Z/evbarm-earmv6hf/binary/gzimg/rpi.img.gz

![](pub/images/NetBSD-smaller-tb.png)




